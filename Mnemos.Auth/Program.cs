﻿using System.IO;
using System.Runtime.CompilerServices;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

using Mnemos.Data;
using Mnemos.Models;
using Mnemos.Models.Interfaces;

namespace Mnemos.Auth {
	internal class Program
	{
		private static IConfiguration _configuration;
		private static IRmqOptions _rmqOptions;

		[ModuleInitializer]
		public static void Init()
		{
			_configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appSettings.json")
				.AddUserSecrets<Program>()
				.Build();

			_rmqOptions = new RmqOptions
			{
				VirtualHost = _configuration["RmqOptions:VirtualHost"],
				UserName = _configuration["RmqOptions:UserName"],
				Password = _configuration["RmqOptions:Password"],
				Host = _configuration["RmqOptions:Host"]
			};
		}

		private static void Main()
		{
			var contextOptions = new DbContextOptionsBuilder<MnemosDbContext>()
				.UseSqlServer(_configuration.GetConnectionString("MnemosDbConnection"))
				.Options;
			using var dbContext = new MnemosDbContext(contextOptions);

			var rpcServer = new RpcServer(_rmqOptions, dbContext);
			rpcServer.Run();
		}
	}
}