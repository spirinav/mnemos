﻿using System;
using System.Linq;
using System.Text;
using System.Text.Json;

using Mnemos.Data;
using Mnemos.Models;
using Mnemos.Models.Interfaces;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

using Serilog;

namespace Mnemos.Auth
{
	public class RpcServer
	{
		private readonly IRmqOptions _rmqOptions;
		private readonly MnemosDbContext _mnemosDbContext;

		public RpcServer(IRmqOptions rmqOptions, MnemosDbContext mnemosDbContext)
		{
			_rmqOptions = rmqOptions;
			_mnemosDbContext = mnemosDbContext;
			Log.Logger = new LoggerConfiguration()
				.WriteTo.File("log.txt")
				.MinimumLevel.Debug()
				.Enrich.FromLogContext()
				.CreateLogger();
		}

		public void Run()
		{
			var factory = new ConnectionFactory
			{
				HostName = _rmqOptions.Host,
				VirtualHost = _rmqOptions.VirtualHost,
				UserName = _rmqOptions.UserName,
				Password = _rmqOptions.Password
			};
			using var connection = factory.CreateConnection();
			using var channel = connection.CreateModel();

			channel.QueueDeclare(
				queue: "rpc_queue",
				durable: false,
				exclusive: false,
				autoDelete: false,
				arguments: null);

			channel.BasicQos(0, 1, false);

			var consumer = new EventingBasicConsumer(channel);
			channel.BasicConsume(queue: "rpc_queue", autoAck: false, consumer: consumer);

			Console.WriteLine(" [x] Awaiting RPC requests");

			consumer.Received += (model, ea) =>
			{
				var response = false;

				var body = ea.Body.ToArray();
				var props = ea.BasicProperties;
				var replyProps = channel.CreateBasicProperties();
				replyProps.CorrelationId = props.CorrelationId;

				try
				{
					var message = Encoding.UTF8.GetString(body);
					var user = (User) JsonSerializer.Deserialize(message, typeof(User));
					response = _mnemosDbContext.Users.Any(u => u.Login == user.Login && u.Password == user.Password);
					Log.Information($"Authentication attempt for {user?.Login}. Result: {response}.");
				}
				catch (Exception ex)
				{
					Log.Error($"Authentication error: {ex}");
				}
				finally
				{
					var responseBytes = Encoding.UTF8.GetBytes(response.ToString());

					channel.BasicPublish(
						exchange: "",
						routingKey: props.ReplyTo,
						basicProperties: replyProps,
						body: responseBytes);

					channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
				}
			};

			Console.WriteLine(" Press [enter] to exit.");
			Console.ReadLine();
		}
	}
}