﻿using System.Collections.Generic;

namespace Mnemos.Models
{
	public class Theme
	{
		public int Id { get; set; }

		public int UserId { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }

		public User User { get; set; }

		public List<Question> Questions { get; set; }
	}
}