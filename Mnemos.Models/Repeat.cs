﻿using System;
using System.Runtime.Serialization;

namespace Mnemos.Models
{
	public class Repeat
	{
		public int Id { get; set; }

		public int QuestionId { get; set; }

		public int RepeatNumber { get; set; }

		public DateTime Date { get; set; }

		public int Difficulty { get; set; }

		public Question Question { get; set; }
	}

	
	[DataContract]
	public enum Difficulty
	{
		[DataMember]
		None = 0,
		
		[DataMember]
		Hard = 1,
		
		[DataMember]
		Good = 2,
		
		[DataMember]
		Easy = 3
	}
}