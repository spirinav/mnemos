﻿using System.Collections.Generic;

namespace Mnemos.Models
{
	public class User
	{
		public int Id { get; set; }

		public string Login { get; set; }

		public string Password { get; set; }

		public string Email { get; set; }

		public long? BotChatId { get; set; }

		public List<Theme> Themes { get; set; }
	}
}