﻿using System;
using System.Collections.Generic;

namespace Mnemos.Models
{
	public class Question
	{
		public int Id { get; set; }

		public int ThemeId { get; set; }

		public string Text { get; set; }

		public string Answer { get; set; }

		public DateTime InsertDate { get; set; }

		public Theme Theme { get; set; }

		public List<Repeat> Repeats { get; set; }
	}
}