﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Mnemos.Models.Interfaces
{
	public interface IGenericRepository<TEntity> where TEntity : class
	{
		IEnumerable<TEntity> GetAll();

		Task<List<TEntity>> GetAllAsync();

		TEntity GetById(int id);

		ValueTask<TEntity> GetByIdAsync(int id);

		IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> expression);

		Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> expression);

		void Add(TEntity entity);

		void AddAsync(TEntity entity);

		void AddRange(IEnumerable<TEntity> entities);

		void AddRangeAsync(IEnumerable<TEntity> entities);

		void Update(TEntity entity);

		void UpdateRange(IEnumerable<TEntity> entities);

		void Remove(TEntity entity);

		void RemoveRange(IEnumerable<TEntity> entities);
	}
}