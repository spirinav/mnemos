﻿namespace Mnemos.Models.Interfaces
{
	public interface IUserRepository : IGenericRepository<User>
	{
	}
}