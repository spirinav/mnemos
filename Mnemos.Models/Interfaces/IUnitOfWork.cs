﻿using System;

namespace Mnemos.Models.Interfaces
{
	public interface IUnitOfWork : IDisposable
	{
		IUserRepository Users { get; }

		IThemeRepository Themes { get; }

		IQuestionRepository Questions { get; }

		IRepeatRepository Repeats { get; }

		int Complete();
	}
}