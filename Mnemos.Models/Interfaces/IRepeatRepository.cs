﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Mnemos.Models.Interfaces
{
	public interface IRepeatRepository : IGenericRepository<Repeat>
	{
		IEnumerable<Repeat> FindWithIncludes(Expression<Func<Repeat, bool>> expression);
	}
}