﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Mnemos.Models.Interfaces
{
	public interface IQuestionRepository : IGenericRepository<Question>
	{
		IEnumerable<Question> FindWithIncludes(Expression<Func<Question, bool>> expression);
	}
}