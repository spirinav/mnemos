﻿namespace Mnemos.Models.Interfaces
{
	public interface IRmqOptions
	{
		string Host { get; }

		string VirtualHost { get; }

		string UserName { get; }

		string Password { get; }
	}
}