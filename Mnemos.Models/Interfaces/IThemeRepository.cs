﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Mnemos.Models.Interfaces
{
	public interface IThemeRepository : IGenericRepository<Theme>
	{
		IEnumerable<Theme> FindWithIncludes(Expression<Func<Theme, bool>> expression);
	}
}