﻿using Mnemos.Models.Interfaces;

namespace Mnemos.Models
{
	public class RmqOptions : IRmqOptions
	{
		public string Host { get; set; }

		public string VirtualHost { get; set; }

		public string UserName { get; set; }

		public string Password { get; set; }
	}
}