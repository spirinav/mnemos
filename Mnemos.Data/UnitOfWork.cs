﻿using Mnemos.Data.Repositories;
using Mnemos.Models.Interfaces;

namespace Mnemos.Data
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly MnemosDbContext _context;

		public IUserRepository Users { get; }

		public IThemeRepository Themes { get; }

		public IQuestionRepository Questions { get; }

		public IRepeatRepository Repeats { get; }

		public UnitOfWork(MnemosDbContext context)
		{
			_context = context;

			Users = new UserRepository(_context);
			Themes = new ThemeRepository(_context);
			Questions = new QuestionRepository(_context);
			Repeats = new RepeatRepository(_context);
		}

		public int Complete()
		{
			return _context.SaveChanges();
		}

		public void Dispose()
		{
			_context.Dispose();
		}
	}
}