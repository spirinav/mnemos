﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Mnemos.Models;

namespace Mnemos.Data.Configurations
{
	public class RepeatConfiguration : IEntityTypeConfiguration<Repeat>
	{
		public void Configure(EntityTypeBuilder<Repeat> builder)
		{
			builder.ToTable("repeats");
			builder.Property(r => r.Id).HasColumnName("id").IsRequired();
			builder.Property(r => r.QuestionId).HasColumnName("question_id").IsRequired();
			builder.Property(r => r.RepeatNumber).HasColumnName("repeat_number").IsRequired();
			builder.Property(r => r.Date).HasColumnName("date").IsRequired();
			builder.Property(r => r.Difficulty).HasColumnName("difficulty").IsRequired().HasDefaultValue(0);
			builder.HasOne(r => r.Question).WithMany(q => q.Repeats).HasForeignKey(r => r.QuestionId)
				.OnDelete(DeleteBehavior.Cascade);
		}
	}
}