﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Mnemos.Models;

namespace Mnemos.Data.Configurations
{
	public class QuestionConfiguration : IEntityTypeConfiguration<Question>
	{
		public void Configure(EntityTypeBuilder<Question> builder)
		{
			builder.ToTable("questions");
			builder.Property(q => q.Id).HasColumnName("id").IsRequired();
			builder.Property(q => q.ThemeId).HasColumnName("theme_id").IsRequired();
			builder.Property(q => q.Text).HasColumnName("text").IsRequired();
			builder.Property(q => q.Answer).HasColumnName("answer").IsRequired();
			builder.Property(q => q.InsertDate).HasColumnName("insert_date").IsRequired()
				.HasDefaultValueSql("getdate()");
			builder.HasOne(q => q.Theme).WithMany(t => t.Questions).HasForeignKey(q => q.ThemeId)
				.OnDelete(DeleteBehavior.Cascade);
		}
	}
}