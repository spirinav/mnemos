﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Mnemos.Models;

namespace Mnemos.Data.Configurations
{
	public class ThemeConfiguration : IEntityTypeConfiguration<Theme>
	{
		public void Configure(EntityTypeBuilder<Theme> builder)
		{
			builder.ToTable("themes");
			builder.Property(t => t.Id).HasColumnName("id").IsRequired();
			builder.Property(t => t.UserId).HasColumnName("user_id").IsRequired();
			builder.Property(t => t.Name).HasColumnName("name").IsRequired().HasMaxLength(200);
			builder.Property(t => t.Description).HasColumnName("description").IsRequired();
			builder.HasOne(t => t.User).WithMany(u => u.Themes).HasForeignKey(t => t.UserId)
				.OnDelete(DeleteBehavior.Cascade);
		}
	}
}