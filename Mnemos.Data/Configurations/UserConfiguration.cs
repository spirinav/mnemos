﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Mnemos.Models;

namespace Mnemos.Data.Configurations
{
	public class UserConfiguration : IEntityTypeConfiguration<User>
	{
		public void Configure(EntityTypeBuilder<User> builder)
		{
			builder.ToTable("users");
			builder.HasIndex(u => u.Login).IsUnique();
			builder.Property(u => u.Id).HasColumnName("id").IsRequired();
			builder.Property(u => u.Login).HasColumnName("login").IsRequired().HasMaxLength(100);
			builder.Property(u => u.Password).HasColumnName("password").IsRequired().HasMaxLength(25);
			builder.Property(u => u.Email).HasColumnName("email").IsRequired().HasMaxLength(100);
			builder.Property(u => u.BotChatId).HasColumnName("bot_chat_id");
		}
	}
}