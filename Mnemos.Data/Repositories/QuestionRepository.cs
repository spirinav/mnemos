﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Microsoft.EntityFrameworkCore;

using Mnemos.Models;
using Mnemos.Models.Interfaces;

namespace Mnemos.Data.Repositories
{
	public class QuestionRepository : GenericRepository<Question>, IQuestionRepository
	{
		public QuestionRepository(MnemosDbContext context) : base(context)
		{
		}

		public IEnumerable<Question> FindWithIncludes(Expression<Func<Question, bool>> expression)
		{
			return Context.Questions
				.Where(expression)
				.Include(q => q.Theme)
				.ThenInclude(t => t.User)
				.Include(q => q.Repeats);
		}
	}
}