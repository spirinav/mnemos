﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Microsoft.EntityFrameworkCore;

using Mnemos.Models;
using Mnemos.Models.Interfaces;

namespace Mnemos.Data.Repositories
{
	public class RepeatRepository : GenericRepository<Repeat>, IRepeatRepository
	{
		public RepeatRepository(MnemosDbContext context) : base(context)
		{
		}

		public IEnumerable<Repeat> FindWithIncludes(Expression<Func<Repeat, bool>> expression)
		{
			return Context.Repeats
				.Where(expression)
				.Include(r => r.Question)
				.ThenInclude(q => q.Theme);
		}
	}
}