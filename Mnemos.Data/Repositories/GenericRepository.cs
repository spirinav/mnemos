﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using Mnemos.Models.Interfaces;

namespace Mnemos.Data.Repositories
{
	public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
	{
		protected readonly MnemosDbContext Context;

		public GenericRepository(MnemosDbContext context)
		{
			Context = context;
		}

		public IEnumerable<TEntity> GetAll()
		{
			return Context.Set<TEntity>().ToList();
		}

		public Task<List<TEntity>> GetAllAsync()
		{
			return Context.Set<TEntity>().ToListAsync();
		}

		public TEntity GetById(int id)
		{
			return Context.Set<TEntity>().Find(id);
		}

		public ValueTask<TEntity> GetByIdAsync(int id)
		{
			return Context.Set<TEntity>().FindAsync(id);
		}

		public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> expression)
		{
			return Context.Set<TEntity>().Where(expression);
		}

		public Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> expression)
		{
			return Context.Set<TEntity>().FirstOrDefaultAsync(expression);
		}

		public void Add(TEntity entity)
		{
			Context.Set<TEntity>().Add(entity);
		}

		public void AddAsync(TEntity entity)
		{
			Context.Set<TEntity>().AddAsync(entity);
		}

		public void AddRange(IEnumerable<TEntity> entities)
		{
			Context.Set<TEntity>().AddRange(entities);
		}

		public void AddRangeAsync(IEnumerable<TEntity> entities)
		{
			Context.Set<TEntity>().AddRangeAsync(entities);
		}

		public void Update(TEntity entity)
		{
			Context.Set<TEntity>().Update(entity);
		}

		public void UpdateRange(IEnumerable<TEntity> entities)
		{
			Context.Set<TEntity>().UpdateRange(entities);
		}

		public void Remove(TEntity entity)
		{
			Context.Set<TEntity>().Remove(entity);
		}

		public void RemoveRange(IEnumerable<TEntity> entities)
		{
			Context.Set<TEntity>().RemoveRange(entities);
		}
	}
}