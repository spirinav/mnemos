﻿using Mnemos.Models;
using Mnemos.Models.Interfaces;

namespace Mnemos.Data.Repositories
{
	public class UserRepository : GenericRepository<User>, IUserRepository
	{
		public UserRepository(MnemosDbContext context) : base(context)
		{
		}
	}
}