﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Microsoft.EntityFrameworkCore;

using Mnemos.Models;
using Mnemos.Models.Interfaces;

namespace Mnemos.Data.Repositories
{
	public class ThemeRepository : GenericRepository<Theme>, IThemeRepository
	{
		public ThemeRepository(MnemosDbContext context) : base(context)
		{
		}

		public IEnumerable<Theme> FindWithIncludes(Expression<Func<Theme, bool>> expression)
		{
			return Context.Themes
				.Where(expression)
				.Include(t => t.Questions)
				.ThenInclude(q => q.Repeats);
		}
	}
}