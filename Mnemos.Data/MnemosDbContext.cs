﻿using Microsoft.EntityFrameworkCore;

using Mnemos.Data.Configurations;
using Mnemos.Models;

namespace Mnemos.Data
{
	public class MnemosDbContext : DbContext
	{
		public DbSet<User> Users { get; set; }

		public DbSet<Theme> Themes { get; set; }

		public DbSet<Question> Questions { get; set; }

		public DbSet<Repeat> Repeats { get; set; }

		public MnemosDbContext(DbContextOptions<MnemosDbContext> options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfiguration(new UserConfiguration());
			modelBuilder.ApplyConfiguration(new ThemeConfiguration());
			modelBuilder.ApplyConfiguration(new QuestionConfiguration());
			modelBuilder.ApplyConfiguration(new RepeatConfiguration());

			base.OnModelCreating(modelBuilder);
		}
	}
}