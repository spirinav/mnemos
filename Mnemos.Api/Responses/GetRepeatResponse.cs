﻿using Mnemos.Models;

namespace Mnemos.Api.Responses
{
	public class GetRepeatResponse
	{
		public Repeat Repeat { get; set; }
	}
}