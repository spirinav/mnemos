﻿namespace Mnemos.Api.Responses
{
	public class CreateNextRepeatResponse
	{
		public bool IsSuccessfullyCreated { get; set; }

		public int ThemeId { get; set; }
	}
}