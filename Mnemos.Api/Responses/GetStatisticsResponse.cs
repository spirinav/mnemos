﻿namespace Mnemos.Api.Responses
{
	public class GetStatisticsResponse
	{
		public int TotalNumberTheme { get; set; }

		public int TotalNumberQuestion { get; set; }

		public int TotalNumberQuestionToRepeat { get; set; }

		public string MaxQuestionTheme { get; set; }

		public string MaxQuestionToRepeatTheme { get; set; }

		public string MostRepeatedQuestion { get; set; }
	}
}