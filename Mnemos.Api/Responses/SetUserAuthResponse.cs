﻿namespace Mnemos.Api.Responses
{
	public class SetUserAuthResponse
	{
		public bool IsSuccessfully { get; set; }
	}
}