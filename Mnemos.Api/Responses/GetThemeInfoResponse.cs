﻿using Mnemos.Models;

namespace Mnemos.Api.Responses
{
	public class GetThemeInfoResponse
	{
		public Theme Theme { get; set; }

		public int TotalQuestions { get; set; }

		public int RepeatQuestions { get; set; }
	}
}