﻿namespace Mnemos.Api.Responses
{
	public class CheckUserAuthResponse
	{
		public bool IsAuth { get; set; }
	}
}