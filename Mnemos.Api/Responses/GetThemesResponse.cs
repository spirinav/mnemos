﻿using System.Collections.Generic;

using Mnemos.Models;

namespace Mnemos.Api.Responses
{
	public class GetThemesResponse
	{
		public List<Theme> Themes { get; set; }
	}
}