﻿using MediatR;

using Mnemos.Api.Responses;

namespace Mnemos.Api.Commands
{
	public class SetUserAuthCommand : IRequest<SetUserAuthResponse>
	{
		public string Login { get; set; }

		public string Password { get; set; }

		public long BotChatId { get; set; }
	}
}