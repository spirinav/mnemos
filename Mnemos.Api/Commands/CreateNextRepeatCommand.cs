﻿using System;

using MediatR;

using Mnemos.Api.Responses;
using Mnemos.Models;

namespace Mnemos.Api.Commands
{
	public class CreateNextRepeatCommand : IRequest<CreateNextRepeatResponse>
	{
		public long BotChatId { get; set; }

		public int RepeatId { get; set; }

		public Difficulty Difficulty { get; set; }

		public int DiffKoeff
		{
			get
			{
				return Difficulty switch
				{
					Difficulty.None => 0,
					Difficulty.Hard => 1,
					Difficulty.Good => 2,
					Difficulty.Easy => 3,
					_ => throw new ArgumentOutOfRangeException()
				};
			}
		}
	}
}