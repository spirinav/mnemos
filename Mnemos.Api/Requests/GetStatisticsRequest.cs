﻿using MediatR;

using Mnemos.Api.Responses;

namespace Mnemos.Api.Requests
{
	public class GetStatisticsRequest : IRequest<GetStatisticsResponse>
	{
		public long BotChatId { get; set; }
	}
}