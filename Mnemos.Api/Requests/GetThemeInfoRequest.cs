﻿using MediatR;

using Mnemos.Api.Responses;

namespace Mnemos.Api.Requests
{
	public class GetThemeInfoRequest : IRequest<GetThemeInfoResponse>
	{
		public long BotChatId { get; set; }

		public int ThemeId { get; set; }
	}
}