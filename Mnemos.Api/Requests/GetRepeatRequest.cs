﻿using MediatR;

using Mnemos.Api.Responses;

namespace Mnemos.Api.Requests
{
	public class GetRepeatRequest : IRequest<GetRepeatResponse>
	{
		public long BotChatId { get; set; }
		
		public int ThemeId { get; set; }
	}
}