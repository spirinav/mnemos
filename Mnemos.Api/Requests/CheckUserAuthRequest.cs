﻿using MediatR;

using Mnemos.Api.Responses;

namespace Mnemos.Api.Requests
{
	public class CheckUserAuthRequest : IRequest<CheckUserAuthResponse>
	{
		public long BotChatId { get; set; }
	}
}