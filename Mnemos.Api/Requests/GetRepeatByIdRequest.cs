﻿using MediatR;

using Mnemos.Api.Responses;

namespace Mnemos.Api.Requests
{
	public class GetRepeatByIdRequest : IRequest<GetRepeatResponse>
	{
		public long BotChatId { get; set; }

		public int RepeatId { get; set; }
	}
}