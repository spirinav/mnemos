﻿using MediatR;

using Mnemos.Api.Responses;

namespace Mnemos.Api.Requests
{
	public class GetThemesRequest : IRequest<GetThemesResponse>
	{
		public long BotChatId { get; set; }

		public bool IsForRepetition { get; set; }
	}
}