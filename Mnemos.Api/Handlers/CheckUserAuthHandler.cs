﻿using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Mnemos.Api.Requests;
using Mnemos.Api.Responses;
using Mnemos.Models.Interfaces;

namespace Mnemos.Api.Handlers
{
	public class CheckUserAuthHandler : IRequestHandler<CheckUserAuthRequest, CheckUserAuthResponse>
	{
		private readonly IUnitOfWork _unitOfWork;

		public CheckUserAuthHandler(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<CheckUserAuthResponse> Handle(CheckUserAuthRequest request,
			CancellationToken cancellationToken)
		{
			var response = new CheckUserAuthResponse();

			var user = await _unitOfWork.Users.FirstOrDefaultAsync(u => u.BotChatId == request.BotChatId);

			response.IsAuth = user != null;

			return await Task.FromResult(response);
		}
	}
}