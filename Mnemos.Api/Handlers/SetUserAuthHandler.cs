﻿using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Mnemos.Api.Commands;
using Mnemos.Api.Responses;
using Mnemos.Models.Interfaces;

namespace Mnemos.Api.Handlers
{
	public class SetUserAuthHandler : IRequestHandler<SetUserAuthCommand, SetUserAuthResponse>
	{
		private readonly IUnitOfWork _unitOfWork;

		public SetUserAuthHandler(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<SetUserAuthResponse> Handle(SetUserAuthCommand command, CancellationToken cancellationToken)
		{
			var response = new SetUserAuthResponse();

			var user = await _unitOfWork.Users.FirstOrDefaultAsync(u =>
				u.Login == command.Login && u.Password == command.Password);

			if (user == null)
			{
				return await Task.FromResult(response);
			}

			user.BotChatId = command.BotChatId;

			_unitOfWork.Users.Update(user);
			var result = _unitOfWork.Complete();

			response.IsSuccessfully = result > 0;

			return await Task.FromResult(response);
		}
	}
}