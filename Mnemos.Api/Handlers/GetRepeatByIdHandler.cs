﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Mnemos.Api.Requests;
using Mnemos.Api.Responses;
using Mnemos.Models.Interfaces;

namespace Mnemos.Api.Handlers
{
	public class GetRepeatByIdHandler : IRequestHandler<GetRepeatByIdRequest, GetRepeatResponse>
	{
		private readonly IUnitOfWork _unitOfWork;

		public GetRepeatByIdHandler(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<GetRepeatResponse> Handle(GetRepeatByIdRequest request, CancellationToken cancellationToken)
		{
			var response = new GetRepeatResponse
			{
				Repeat = _unitOfWork.Repeats.FindWithIncludes(r =>
						r.Id == request.RepeatId &&
						r.Question.Theme.User.BotChatId == request.BotChatId)
					.FirstOrDefault()
			};

			return await Task.FromResult(response);
		}
	}
}