﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Mnemos.Api.Requests;
using Mnemos.Api.Responses;
using Mnemos.Models.Interfaces;

namespace Mnemos.Api.Handlers
{
	public class GetThemesHandler : IRequestHandler<GetThemesRequest, GetThemesResponse>
	{
		private readonly IUnitOfWork _unitOfWork;

		public GetThemesHandler(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<GetThemesResponse> Handle(GetThemesRequest request, CancellationToken cancellationToken)
		{
			var getThemesResponse = new GetThemesResponse();

			if (request.IsForRepetition)
			{
				var repeats = _unitOfWork.Repeats
					.FindWithIncludes(r => r.Date <= DateTime.Now && r.Difficulty == 0)
					.ToList();

				getThemesResponse.Themes = repeats.Select(r => r.Question.Theme).Distinct().ToList();
			}
			else
			{
				getThemesResponse.Themes = _unitOfWork.Themes.Find(t => t.User.BotChatId == request.BotChatId).ToList();
			}

			return await Task.FromResult(getThemesResponse);
		}
	}
}