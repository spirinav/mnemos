﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Mnemos.Api.Requests;
using Mnemos.Api.Responses;
using Mnemos.Models.Interfaces;

namespace Mnemos.Api.Handlers
{
	public class GetThemeInfoHandler : IRequestHandler<GetThemeInfoRequest, GetThemeInfoResponse>
	{
		private readonly IUnitOfWork _unitOfWork;

		public GetThemeInfoHandler(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<GetThemeInfoResponse> Handle(GetThemeInfoRequest request, CancellationToken cancellationToken)
		{
			var response = new GetThemeInfoResponse();

			var theme = _unitOfWork.Themes
				.FindWithIncludes(t => t.Id == request.ThemeId && t.User.BotChatId == request.BotChatId)
				.First();

			if (theme == null)
			{
				return await Task.FromResult(response);
			}

			response.Theme = theme;
			response.TotalQuestions = theme.Questions.Count;
			response.RepeatQuestions = theme.Questions.SelectMany(q => q.Repeats)
				.Count(r => r.Date <= DateTime.Now && r.Difficulty == 0);

			return await Task.FromResult(response);
		}
	}
}