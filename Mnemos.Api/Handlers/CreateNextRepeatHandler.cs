﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Mnemos.Api.Commands;
using Mnemos.Api.Responses;
using Mnemos.Models;
using Mnemos.Models.Interfaces;

namespace Mnemos.Api.Handlers
{
	public class CreateNextRepeatHandler : IRequestHandler<CreateNextRepeatCommand, CreateNextRepeatResponse>
	{
		private readonly IUnitOfWork _unitOfWork;

		public CreateNextRepeatHandler(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<CreateNextRepeatResponse> Handle(CreateNextRepeatCommand command,
			CancellationToken cancellationToken)
		{
			var response = new CreateNextRepeatResponse();

			var repeat = _unitOfWork.Repeats.FindWithIncludes(r =>
				r.Id == command.RepeatId && r.Question.Theme.User.BotChatId == command.BotChatId).First();
			if (repeat == null || command.Difficulty == Difficulty.None)
			{
				return await Task.FromResult(response);
			}

			repeat.Difficulty = command.DiffKoeff;
			_unitOfWork.Repeats.Update(repeat);

			var nextRepeat = new Repeat
			{
				QuestionId = repeat.QuestionId,
				RepeatNumber = command.Difficulty == Difficulty.Hard ? 0 : repeat.RepeatNumber + 1
			};

			nextRepeat.Date = DateTime.Now.Date.AddDays(command.DiffKoeff * nextRepeat.RepeatNumber + 1);

			_unitOfWork.Repeats.Add(nextRepeat);
			var result = _unitOfWork.Complete();

			response.IsSuccessfullyCreated = result > 0;
			response.ThemeId = repeat.Question.ThemeId;

			return await Task.FromResult(response);
		}
	}
}