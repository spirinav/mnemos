﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Mnemos.Api.Requests;
using Mnemos.Api.Responses;
using Mnemos.Models.Interfaces;

namespace Mnemos.Api.Handlers
{
	public class GetRepeatHandler : IRequestHandler<GetRepeatRequest, GetRepeatResponse>
	{
		private readonly IUnitOfWork _unitOfWork;

		public GetRepeatHandler(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<GetRepeatResponse> Handle(GetRepeatRequest request, CancellationToken cancellationToken)
		{
			var response = new GetRepeatResponse();

			var repeats = _unitOfWork.Repeats.FindWithIncludes(r => r.Question.Theme.Id == request.ThemeId);

			var nextRepeat = repeats?
				.Where(r => r.Date <= DateTime.Now && r.Difficulty == 0)
				.OrderBy(r => r.Date)
				.FirstOrDefault();

			response.Repeat = nextRepeat;

			return await Task.FromResult(response);
		}
	}
}