﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Mnemos.Api.Requests;
using Mnemos.Api.Responses;
using Mnemos.Models.Interfaces;

namespace Mnemos.Api.Handlers
{
	public class GetStatisticsHandler : IRequestHandler<GetStatisticsRequest, GetStatisticsResponse>
	{
		private readonly IUnitOfWork _unitOfWork;

		public GetStatisticsHandler(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<GetStatisticsResponse> Handle(GetStatisticsRequest request,
			CancellationToken cancellationToken)
		{
			var response = new GetStatisticsResponse();

			var themes = _unitOfWork.Themes
				.FindWithIncludes(t => t.User.BotChatId == request.BotChatId)
				.ToList();

			response.TotalNumberTheme = themes.Count;
			response.TotalNumberQuestion = themes.SelectMany(t => t.Questions).Count();
			response.TotalNumberQuestionToRepeat = themes.SelectMany(t => t.Questions)
				.SelectMany(q => q.Repeats)
				.Count(r => r.Date <= DateTime.Now && r.Difficulty == 0);
			response.MaxQuestionTheme = themes.Select(t => new {Theme = t, t.Questions.Count})
				.OrderByDescending(x => x.Count)
				.FirstOrDefault()?
				.Theme?.Name;
			response.MaxQuestionToRepeatTheme = themes.Select(t => new
				{
					Theme = t,
					Count = t.Questions.SelectMany(q => q.Repeats)
						.Count(r => r.Date <= DateTime.Now && r.Difficulty == 0)
				})
				.Where(x => x.Count > 0)
				.OrderByDescending(x => x.Count)
				.FirstOrDefault()?
				.Theme?.Name;
			response.MostRepeatedQuestion = themes.SelectMany(t => t.Questions).Select(q => new
				{
					Question = q.Text,
					q.Repeats.Count
				}).Where(x => x.Count > 0)
				.OrderByDescending(x => x.Count)
				.FirstOrDefault()?
				.Question;

			return await Task.FromResult(response);
		}
	}
}