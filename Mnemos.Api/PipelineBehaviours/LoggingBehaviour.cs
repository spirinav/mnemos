﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Serilog;

namespace Mnemos.Api.PipelineBehaviours
{
	public class LoggingBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
		where TRequest : IRequest<TResponse>
	{
		public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken,
			RequestHandlerDelegate<TResponse> next)
		{
			Log.Information($"Handling {typeof(TRequest).Name}");
			var type = request.GetType();
			IList<PropertyInfo> props = new List<PropertyInfo>(type.GetProperties());
			foreach (var prop in props)
			{
				if (prop.Name == "Password")
				{
					continue;
				}

				var propValue = prop.GetValue(request, null);
				Log.Information("{Property} : {@Value}", prop.Name, propValue);
			}

			var response = await next();

			Log.Information($"Handled {typeof(TResponse).Name}");
			type = response.GetType();
			props = new List<PropertyInfo>(type.GetProperties());
			foreach (var prop in props)
			{
				var propValue = prop.GetValue(response, null);
				Log.Information("{Property} : {@Value}", prop.Name, propValue);
			}

			return response;
		}
	}
}