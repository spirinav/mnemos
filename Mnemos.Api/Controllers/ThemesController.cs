﻿using System.Threading.Tasks;

using MediatR;

using Microsoft.AspNetCore.Mvc;

using Mnemos.Api.Requests;
using Mnemos.Api.Responses;

using Swashbuckle.AspNetCore.Annotations;

namespace Mnemos.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ThemesController : ControllerBase
	{
		private readonly IMediator _mediator;

		public ThemesController(IMediator mediator)
		{
			_mediator = mediator;
		}

		[HttpGet("GetThemes")]
		[SwaggerOperation(OperationId = "GetThemes")]
		public async Task<ActionResult<GetThemesResponse>> GetThemes([FromQuery] GetThemesRequest request) =>
			Ok(await _mediator.Send(request));

		[HttpGet("GetThemeInfo")]
		[SwaggerOperation(OperationId = "GetThemeInfo")]
		public async Task<ActionResult<GetThemeInfoResponse>> GetThemeInfo([FromQuery] GetThemeInfoRequest request) =>
			Ok(await _mediator.Send(request));
	}
}