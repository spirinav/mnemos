﻿using System.Threading.Tasks;

using MediatR;

using Microsoft.AspNetCore.Mvc;

using Mnemos.Api.Commands;
using Mnemos.Api.Requests;
using Mnemos.Api.Responses;

using Swashbuckle.AspNetCore.Annotations;

namespace Mnemos.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class RepeatsController : ControllerBase
	{
		private readonly IMediator _mediator;

		public RepeatsController(IMediator mediator)
		{
			_mediator = mediator;
		}

		[HttpGet("GetRepeat")]
		[SwaggerOperation(OperationId = "GetRepeat")]
		public async Task<ActionResult<GetRepeatResponse>> GetRepeat([FromQuery] GetRepeatRequest request) =>
			Ok(await _mediator.Send(request));

		[HttpGet("GetRepeatById")]
		[SwaggerOperation(OperationId = "GetRepeatById")]
		public async Task<ActionResult<GetRepeatResponse>> GetRepeatById([FromQuery] GetRepeatByIdRequest request) =>
			Ok(await _mediator.Send(request));

		[HttpPost("CreateNextRepeat")]
		[SwaggerOperation(OperationId = "CreateNextRepeat")]
		public async Task<ActionResult<CreateNextRepeatResponse>> CreateNextRepeat(CreateNextRepeatCommand command) =>
			Ok(await _mediator.Send(command));
	}
}