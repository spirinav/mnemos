﻿using System.Threading.Tasks;

using MediatR;

using Microsoft.AspNetCore.Mvc;

using Mnemos.Api.Commands;
using Mnemos.Api.Requests;
using Mnemos.Api.Responses;

using Swashbuckle.AspNetCore.Annotations;

namespace Mnemos.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UsersController : ControllerBase
	{
		private readonly IMediator _mediator;

		public UsersController(IMediator mediator)
		{
			_mediator = mediator;
		}

		[HttpGet("CheckUserAuth")]
		[SwaggerOperation(OperationId = "CheckUserAuth")]
		public async Task<ActionResult<CheckUserAuthResponse>> CheckUserAuth([FromQuery] CheckUserAuthRequest request) =>
			Ok(await _mediator.Send(request));

		[HttpPost("SetUserAuth")]
		[SwaggerOperation(OperationId = "SetUserAuth")]
		public async Task<ActionResult<SetUserAuthResponse>> SetUserAuth(SetUserAuthCommand command) =>
			Ok(await _mediator.Send(command));

		[HttpGet("GetStatistics")]
		[SwaggerOperation(OperationId = "GetStatistics")]
		public async Task<ActionResult<GetStatisticsResponse>> GetStatistics([FromQuery] GetStatisticsRequest request) =>
			Ok(await _mediator.Send(request));
	}
}