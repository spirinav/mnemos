using MediatR;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using Mnemos.Api.PipelineBehaviours;
using Mnemos.Data;
using Mnemos.Data.Repositories;
using Mnemos.Models.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Mnemos.Api
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<MnemosDbContext>(options =>
				options.UseSqlServer(Configuration.GetConnectionString("MnemosDbConnection")));

			services.AddControllers()
				.AddNewtonsoftJson(options =>
					{
						options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
						options.SerializerSettings.Converters.Add(new StringEnumConverter());
					}
				);

			services.AddSwaggerGen(
				x =>
				{
					x.SwaggerDoc(
						"v1",
						new OpenApiInfo
						{
							Title = "Mnemos.Api",
							Version = "v1"
						});

					x.EnableAnnotations();
				});
			services.AddSwaggerGenNewtonsoftSupport();

			services.AddMediatR(typeof(Startup));
			services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehaviour<,>));

			services.AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>));
			services.AddTransient<IUserRepository, UserRepository>();
			services.AddTransient<IThemeRepository, ThemeRepository>();
			services.AddTransient<IQuestionRepository, QuestionRepository>();
			services.AddTransient<IRepeatRepository, RepeatRepository>();
			services.AddTransient<IUnitOfWork, UnitOfWork>();
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseSwagger();
				app.UseSwaggerUI(x => x.SwaggerEndpoint("/swagger/v1/swagger.json", "Mnemos.Api v1"));
			}

			app.UseRouting();

			app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
		}
	}
}