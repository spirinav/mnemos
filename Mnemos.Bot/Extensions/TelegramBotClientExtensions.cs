﻿using System.Threading.Tasks;

using Mnemos.Bot.Handlers;

using Telegram.Bot;
using Telegram.Bot.Types;

namespace Mnemos.Bot.Extensions
{
	public static class TelegramBotClientExtensions
	{
		public static async Task HandleMessage(this ITelegramBotClient bot, Message message)
		{
			await bot.GetMessageHandler(message).Handle();
		}

		public static BaseMessageHandler GetMessageHandler(this ITelegramBotClient bot, Message message)
		{
			if (message.ReplyToMessage?.Text == "Enter login and password")
			{
				return new LoginPasswordMessageHandler(bot, message);
			}

			return message.Text switch
			{
				"/start" => new StartMessageHandler(bot, message),
				"/sign_in" => new SignInMessageHandler(bot, message),
				"/get_themes" => new GetThemesMessageHandler(bot, message),
				"/get_themes_to_repeat" => new GetThemesToRepeatMessageHandler(bot, message),
				"/get_statistics" => new GetStatisticsMessageHandler(bot, message),
				_ => new UnknownMessageHandler(bot, message)
			};
		}

		public static async Task HandleCallbackQuery(this ITelegramBotClient bot, CallbackQuery query)
		{
			await bot.GetCallbackQueryHandler(query).Handle();
		}

		public static BaseCallbackQueryHandler GetCallbackQueryHandler(this ITelegramBotClient bot, CallbackQuery query)
		{
			if (query?.Data?.StartsWith("/get_theme_info") ?? false)
			{
				return new GetThemeInfoCallbackQueryHandler(bot, query);
			}

			if (query?.Data?.StartsWith("/repeat") ?? false)
			{
				return new RepeatCallbackQueryHandler(bot, query);
			}

			if (query?.Data?.StartsWith("/answer") ?? false)
			{
				return new AnswerCallbackQueryHandler(bot, query);
			}

			if (query?.Data?.StartsWith("/create_new_repeat") ?? false)
			{
				return new CreateNewRepeatCallbackQueryHandler(bot, query);
			}

			if (query?.Data?.StartsWith("/get_themes_to_repeate") ?? false)
			{
				return new GetThemesToRepeatCallbackQueryHandler(bot, query);
			}

			return new UnknownCallbackQueryHandler(bot, query);
		}
	}
}