﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using Mnemos.Bot.Extensions;

using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;

namespace Mnemos.Bot
{
	internal class Program
	{
		private static ITelegramBotClient _bot;

		[ModuleInitializer]
		public static async void BotInit()
		{
			var config = new ConfigurationBuilder()
				.AddUserSecrets<Program>()
				.Build();

			_bot = new TelegramBotClient(config["ApiToken"]);

			await _bot.SetMyCommandsAsync(
				new List<BotCommand>()
				{
					new() {Command = "sign_in", Description = "Sign in to mnemos_bot"},
					new() {Command = "get_themes", Description = "Get all themes"},
					new() {Command = "get_themes_to_repeat", Description = "Get themes to repeat"},
					new() {Command = "get_statistics", Description = "Get statistics"}
				}, BotCommandScope.Default());
		}

		private static void Main()
		{
			Console.WriteLine(_bot.GetMeAsync().Result.FirstName);

			_bot.StartReceiving(HandleUpdateAsync, HandleErrorAsync);

			Console.WriteLine("Press [Enter] to exit.");
			Console.ReadLine();
		}

		private static async Task HandleUpdateAsync(ITelegramBotClient bot, Update update,
			CancellationToken cancellationToken)
		{
			if (update.Message is { } message)
			{
				await bot.HandleMessage(message);
			}

			if (update.CallbackQuery is { } query)
			{
				await bot.HandleCallbackQuery(query);
			}
		}

		private static Task HandleErrorAsync(ITelegramBotClient bot, Exception exception,
			CancellationToken cancellationToken)
		{
			var errorMessage = exception switch
			{
				ApiRequestException apiRequestException
					=> $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
				_ => exception.ToString()
			};

			Console.WriteLine(errorMessage);
			return Task.CompletedTask;
		}
	}
}