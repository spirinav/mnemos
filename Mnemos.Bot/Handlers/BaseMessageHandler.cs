﻿using System.Threading.Tasks;

using Telegram.Bot;
using Telegram.Bot.Types;

namespace Mnemos.Bot.Handlers
{
	public abstract class BaseMessageHandler
	{
		protected const string BaseAddress = "http://localhost:8941";
		protected readonly ITelegramBotClient Bot;
		protected readonly Message Message;

		protected BaseMessageHandler(ITelegramBotClient bot, Message message)
		{
			Bot = bot;
			Message = message;
		}

		public abstract Task Handle();
	}
}