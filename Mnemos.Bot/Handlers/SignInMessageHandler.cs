﻿using System.Threading.Tasks;

using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace Mnemos.Bot.Handlers
{
	public class SignInMessageHandler : BaseMessageHandler
	{
		public SignInMessageHandler(ITelegramBotClient bot, Message message) : base(bot, message)
		{
		}

		public override async Task Handle()
		{
			await Bot.SendTextMessageAsync(Message.Chat.Id, "Enter login and password",
				replyMarkup: new ForceReplyMarkup {InputFieldPlaceholder = "[login] [password]"});
		}
	}
}