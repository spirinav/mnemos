﻿using System.Threading.Tasks;

using IO.Swagger.Api;

using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace Mnemos.Bot.Handlers
{
	public class RepeatCallbackQueryHandler : BaseCallbackQueryHandler
	{
		public RepeatCallbackQueryHandler(ITelegramBotClient bot, CallbackQuery query) : base(bot, query)
		{
		}

		public override async Task Handle()
		{
			var chatId = Query?.Message?.Chat.Id;
			var array = Query?.Data?.Split(" ");
			if (chatId != null && array?.Length >= 2 && int.TryParse(array[1], out var themeId))
			{
				var getRepeatResponse = await new RepeatsApi(BaseAddress).GetRepeatAsync(chatId, themeId);

				var repeat = getRepeatResponse?.Repeat;
				if (repeat != null)
				{
					var inlineKeyboard = new InlineKeyboardMarkup(
						new InlineKeyboardButton("Show answer")
						{
							CallbackData = $"/answer {repeat.Id}"
						});

					await Bot.SendTextMessageAsync(
						chatId,
						repeat.Question.Text,
						replyMarkup: inlineKeyboard);
				}
				else
				{
					var inlineKeyboard = new InlineKeyboardMarkup(
						new InlineKeyboardButton("Show themes to repeat")
						{
							CallbackData = "/get_themes_to_repeate"
						});

					await Bot.SendTextMessageAsync(
						chatId,
						"There are no questions to repeat in this theme.",
						replyMarkup: inlineKeyboard);
				}
			}
		}
	}
}