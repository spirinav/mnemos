﻿using System.Threading.Tasks;

using Telegram.Bot;
using Telegram.Bot.Types;

namespace Mnemos.Bot.Handlers
{
	public class UnknownCallbackQueryHandler : BaseCallbackQueryHandler
	{
		public UnknownCallbackQueryHandler(ITelegramBotClient bot, CallbackQuery query) : base(bot, query)
		{
		}

		public override async Task Handle()
		{
			var chatId = Query?.Message?.Chat.Id ?? 0;
			await Bot.SendTextMessageAsync(chatId, "You have entered an unknown command.\n" +
			                                       "The list of supported commands is available in the Menu.");
		}
	}
}