﻿using System.Threading.Tasks;

using IO.Swagger.Api;
using IO.Swagger.Model;

using Telegram.Bot;
using Telegram.Bot.Types;

namespace Mnemos.Bot.Handlers
{
	public class LoginPasswordMessageHandler : BaseMessageHandler
	{
		public LoginPasswordMessageHandler(ITelegramBotClient bot, Message message) : base(bot, message)
		{
		}

		public override async Task Handle()
		{
			var array = Message.Text?.Split(" ");
			if (array?.Length >= 2)
			{
				var setUserAuthCommand = new SetUserAuthCommand
				{
					Login = array[0],
					Password = array[1],
					BotChatId = Message.Chat.Id
				};

				var setUserAuthResponse = await new UsersApi(BaseAddress).SetUserAuthAsync(setUserAuthCommand);
				if (setUserAuthResponse?.IsSuccessfully ?? false)
				{
					await new StartMessageHandler(Bot, Message).Handle();
					return;
				}
			}

			await Bot.SendTextMessageAsync(Message.Chat.Id, "Incorrect login and/or password.");
			await new SignInMessageHandler(Bot, Message).Handle();
		}
	}
}