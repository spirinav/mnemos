﻿using System.Threading.Tasks;

using IO.Swagger.Api;

using Telegram.Bot;
using Telegram.Bot.Types;

namespace Mnemos.Bot.Handlers
{
	public class RepeatMessageHandler : BaseMessageHandler
	{
		public RepeatMessageHandler(ITelegramBotClient bot, Message message) : base(bot, message)
		{
		}

		public override async Task Handle()
		{
			var msg = Message.Text != null ? Message.Text.Split(" ") : new string[] { };
			if (msg.Length >= 2 && int.TryParse(msg[1], out var themeId))
			{
				var getRepeatResponse = await new RepeatsApi(BaseAddress).GetRepeatAsync(Message.Chat.Id, themeId);

				if (getRepeatResponse is {Repeat: { }})
				{
					await Bot.SendTextMessageAsync(Message.Chat.Id, getRepeatResponse.Repeat.Question.Text);
					return;
				}
			}

			await Bot.SendTextMessageAsync(
				Message.Chat.Id,
				"Something went wrong. Try again.\n/themeinfo [theme_id]");
		}
	}
}