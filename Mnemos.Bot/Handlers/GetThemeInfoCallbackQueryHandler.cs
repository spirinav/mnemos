﻿using System.Threading.Tasks;

using IO.Swagger.Api;

using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace Mnemos.Bot.Handlers
{
	public class GetThemeInfoCallbackQueryHandler : BaseCallbackQueryHandler
	{
		public GetThemeInfoCallbackQueryHandler(ITelegramBotClient bot, CallbackQuery query) : base(bot, query)
		{
		}

		public override async Task Handle()
		{
			var chatId = Query?.Message?.Chat.Id;
			var array = Query?.Data?.Split(" ");
			if (chatId != null && array?.Length >= 2 && int.TryParse(array[1], out var themeId))
			{
				var getThemeInfoResponse = await new ThemesApi(BaseAddress).GetThemeInfoAsync(chatId, themeId);

				var inlineKeyboard = getThemeInfoResponse.RepeatQuestions > 0
					? new InlineKeyboardMarkup(
						new InlineKeyboardButton("Repeat theme")
						{
							CallbackData = $"/repeat {themeId}"
						})
					: null;

				await Bot.SendTextMessageAsync(
					chatId,
					$"*Id:* {getThemeInfoResponse.Theme.Id}" +
					$"\n*Name:* {getThemeInfoResponse.Theme.Name}" +
					$"\n*Description:* {getThemeInfoResponse.Theme.Description}" +
					$"\n*Total questions:* {getThemeInfoResponse.TotalQuestions}" +
					$"\n*Questions to repeat now:* {getThemeInfoResponse.RepeatQuestions}",
					ParseMode.Markdown,
					replyMarkup: inlineKeyboard);
			}
		}
	}
}