﻿using System;
using System.Threading.Tasks;

using IO.Swagger.Api;
using IO.Swagger.Model;

using Telegram.Bot;
using Telegram.Bot.Types;

namespace Mnemos.Bot.Handlers
{
	public class CreateNewRepeatCallbackQueryHandler : BaseCallbackQueryHandler
	{
		public CreateNewRepeatCallbackQueryHandler(ITelegramBotClient bot, CallbackQuery query) : base(bot, query)
		{
		}

		public override async Task Handle()
		{
			var chatId = Query?.Message?.Chat.Id;
			var array = Query?.Data?.Split(" ");
			if (chatId != null && array?.Length >= 2 &&
			    int.TryParse(array[1], out var repeatId) &&
			    Enum.TryParse(array[2], true, out Difficulty difficulty))
			{
				var createNextRepeatCommand = new CreateNextRepeatCommand
				{
					BotChatId = chatId,
					RepeatId = repeatId,
					Difficulty = difficulty
				};

				var createNextRepeatResponse = await new RepeatsApi(BaseAddress)
					.CreateNextRepeatAsync(createNextRepeatCommand);

				var nextRepeatQuery = new CallbackQuery
				{
					Message = Query.Message,
					Data = $"/repeat {createNextRepeatResponse.ThemeId}"
				};
				await new RepeatCallbackQueryHandler(Bot, nextRepeatQuery).Handle();
			}
		}
	}
}