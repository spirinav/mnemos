﻿using System.Threading.Tasks;

using Telegram.Bot;
using Telegram.Bot.Types;

namespace Mnemos.Bot.Handlers
{
	public class UnknownMessageHandler : BaseMessageHandler
	{
		public UnknownMessageHandler(ITelegramBotClient bot, Message message) : base(bot, message)
		{
		}

		public override async Task Handle()
		{
			await Bot.SendTextMessageAsync(Message.Chat, "You have entered an unknown command.\n" +
			                                             "The list of supported commands is available in the Menu.");
		}
	}
}