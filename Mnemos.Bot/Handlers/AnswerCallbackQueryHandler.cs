﻿using System.Collections.Generic;
using System.Threading.Tasks;

using IO.Swagger.Api;
using IO.Swagger.Model;

using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace Mnemos.Bot.Handlers
{
	public class AnswerCallbackQueryHandler : BaseCallbackQueryHandler
	{
		public AnswerCallbackQueryHandler(ITelegramBotClient bot, CallbackQuery query) : base(bot, query)
		{
		}

		public override async Task Handle()
		{
			var chatId = Query?.Message?.Chat.Id;
			var array = Query?.Data?.Split(" ");
			if (chatId != null && array?.Length >= 2 && int.TryParse(array[1], out var repeatId))
			{
				var getRepeatResponse = await new RepeatsApi(BaseAddress).GetRepeatByIdAsync(chatId, repeatId);
				
				var inlineKeyboard = new InlineKeyboardMarkup(
					new List<InlineKeyboardButton>()
					{
						new(Difficulty.Hard.ToString()) {CallbackData = $"/create_new_repeat {repeatId} {Difficulty.Hard}"},
						new(Difficulty.Good.ToString()) {CallbackData = $"/create_new_repeat {repeatId} {Difficulty.Good}"},
						new(Difficulty.Easy.ToString()) {CallbackData = $"/create_new_repeat {repeatId} {Difficulty.Easy}"}
					});

				await Bot.SendTextMessageAsync(
					chatId,
					getRepeatResponse.Repeat.Question.Answer,
					replyMarkup: inlineKeyboard);
			}
		}
	}
}