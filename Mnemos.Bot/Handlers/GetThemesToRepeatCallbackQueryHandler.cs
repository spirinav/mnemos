﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IO.Swagger.Api;

using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace Mnemos.Bot.Handlers
{
	public class GetThemesToRepeatCallbackQueryHandler : BaseCallbackQueryHandler
	{
		public GetThemesToRepeatCallbackQueryHandler(ITelegramBotClient bot, CallbackQuery query) : base(bot, query)
		{
		}

		public override async Task Handle()
		{
			var chatId = Query?.Message?.Chat.Id;
			if (chatId == null)
			{
				return;
			}

			var getThemesResponse = await new ThemesApi(BaseAddress).GetThemesAsync(chatId, true);

			if (getThemesResponse.Themes?.Any() ?? false)
			{
				var inlineKeyboardButtons = new List<List<InlineKeyboardButton>>();
				getThemesResponse.Themes.ForEach(t =>
					inlineKeyboardButtons.Add(new List<InlineKeyboardButton>
					{
						InlineKeyboardButton.WithCallbackData(t.Name, $"/get_theme_info {t.Id}")
					}));

				var inlineKeyboard = new InlineKeyboardMarkup(inlineKeyboardButtons);

				await Bot.SendTextMessageAsync(chatId, "Your themes to repeat:", replyMarkup: inlineKeyboard);
			}
			else
			{
				await Bot.SendTextMessageAsync(chatId, "You don't have any themes to repeat yet.");
			}
		}
	}
}