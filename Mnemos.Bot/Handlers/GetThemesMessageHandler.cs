﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IO.Swagger.Api;

using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace Mnemos.Bot.Handlers
{
	public class GetThemesMessageHandler : BaseMessageHandler
	{
		public GetThemesMessageHandler(ITelegramBotClient bot, Message message) : base(bot, message)
		{
		}

		public override async Task Handle()
		{
			var getThemesResponse = await new ThemesApi(BaseAddress).GetThemesAsync(Message.Chat.Id, false);
			if (getThemesResponse.Themes?.Any() ?? false)
			{
				var inlineKeyboardButtons = new List<List<InlineKeyboardButton>>();
				getThemesResponse.Themes.ForEach(t =>
					inlineKeyboardButtons.Add(new List<InlineKeyboardButton>
					{
						InlineKeyboardButton.WithCallbackData(t.Name, $"/get_theme_info {t.Id}")
					}));

				var inlineKeyboard = new InlineKeyboardMarkup(inlineKeyboardButtons);

				await Bot.SendTextMessageAsync(Message.Chat.Id, "Your themes:", replyMarkup: inlineKeyboard);
			}
			else
			{
				await Bot.SendTextMessageAsync(Message.Chat.Id, "You don't have any themes yet.");
			}
		}
	}
}