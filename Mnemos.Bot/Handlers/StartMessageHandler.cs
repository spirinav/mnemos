﻿using System.Threading.Tasks;

using IO.Swagger.Api;

using Telegram.Bot;
using Telegram.Bot.Types;

namespace Mnemos.Bot.Handlers
{
	public class StartMessageHandler : BaseMessageHandler
	{
		public StartMessageHandler(ITelegramBotClient bot, Message message) : base(bot, message)
		{
		}

		public override async Task Handle()
		{
			var checkUserAuthResponse = await new UsersApi(BaseAddress).CheckUserAuthAsync(Message.Chat.Id);
			if (checkUserAuthResponse?.IsAuth ?? false)
			{
				await Bot.SendTextMessageAsync(Message.Chat.Id, $"Welcome to MnemosBot, {Message.Chat.FirstName}!");

				return;
			}

			await new SignInMessageHandler(Bot, Message).Handle();
		}
	}
}