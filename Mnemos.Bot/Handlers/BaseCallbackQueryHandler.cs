﻿using System.Threading.Tasks;

using Telegram.Bot;
using Telegram.Bot.Types;

namespace Mnemos.Bot.Handlers
{
	public abstract class BaseCallbackQueryHandler
	{
		protected const string BaseAddress = "http://localhost:8941";
		protected readonly ITelegramBotClient Bot;
		protected readonly CallbackQuery Query;

		protected BaseCallbackQueryHandler(ITelegramBotClient bot, CallbackQuery query)
		{
			Bot = bot;
			Query = query;
		}

		public abstract Task Handle();
	}
}