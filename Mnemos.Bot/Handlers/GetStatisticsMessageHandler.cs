﻿using System.Threading.Tasks;

using IO.Swagger.Api;

using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Mnemos.Bot.Handlers
{
	public class GetStatisticsMessageHandler : BaseMessageHandler
	{
		public GetStatisticsMessageHandler(ITelegramBotClient bot, Message message) : base(bot, message)
		{
		}

		public override async Task Handle()
		{
			var getStatisticsResponse = await new UsersApi(BaseAddress).GetStatisticsAsync(Message.Chat.Id);

			await Bot.SendTextMessageAsync(
				Message.Chat.Id,
				$"*Total number of themes:* {getStatisticsResponse.TotalNumberTheme}" +
				$"\n*Total number of questions:* {getStatisticsResponse.TotalNumberQuestion}" +
				$"\n*Total number of questions to repeat:* {getStatisticsResponse.TotalNumberQuestionToRepeat}" +
				$"\n*Theme with the most questions:* {getStatisticsResponse.MaxQuestionTheme}" +
				$"\n*Theme with the most questions to repeat:* {getStatisticsResponse.MaxQuestionToRepeatTheme}" +
				$"\n*Most repeated question:* {getStatisticsResponse.MostRepeatedQuestion}",
				ParseMode.Markdown
			);
		}
	}
}