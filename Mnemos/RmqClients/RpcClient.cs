﻿using System;
using System.Collections.Concurrent;
using System.Text;
using System.Text.Json;

using Microsoft.Extensions.Configuration;

using Mnemos.Models;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Mnemos.RmqClients
{
	public class RpcClient
	{
		private readonly IConnection _connection;
		private readonly IModel _channel;
		private readonly string _replyQueueName;
		private readonly EventingBasicConsumer _consumer;
		private readonly BlockingCollection<string> _respQueue = new();
		private readonly IBasicProperties _props;

		public RpcClient()
		{
			var rmqOptions = GetRmqOptions();
			var factory = new ConnectionFactory
			{
				HostName = rmqOptions.Host,
				VirtualHost = rmqOptions.VirtualHost,
				UserName = rmqOptions.UserName,
				Password = rmqOptions.Password
			};
			_connection = factory.CreateConnection();
			_channel = _connection.CreateModel();
			_replyQueueName = _channel.QueueDeclare().QueueName;
			_consumer = new EventingBasicConsumer(_channel);

			_props = _channel.CreateBasicProperties();
			var correlationId = Guid.NewGuid().ToString();
			_props.CorrelationId = correlationId;
			_props.ReplyTo = _replyQueueName;

			_consumer.Received += (model, ea) =>
			{
				var body = ea.Body.ToArray();
				var response = Encoding.UTF8.GetString(body);
				if (ea.BasicProperties.CorrelationId == correlationId)
				{
					_respQueue.Add(response);
				}
			};
		}

		private RmqOptions GetRmqOptions()
		{
			var configuration = new ConfigurationBuilder()
				.AddUserSecrets<RpcClient>()
				.Build();

			var rmqOptions = new RmqOptions
			{
				Host = configuration["RmqOptions:Host"],
				VirtualHost = configuration["RmqOptions:VirtualHost"],
				UserName = configuration["RmqOptions:UserName"],
				Password = configuration["RmqOptions:Password"]
			};

			return rmqOptions;
		}

		public bool Call(User request)
		{
			var message = JsonSerializer.Serialize(request, typeof(User));
			var messageBytes = Encoding.UTF8.GetBytes(message);

			_channel.BasicPublish(
				exchange: "",
				routingKey: "rpc_queue",
				basicProperties: _props,
				body: messageBytes);

			_channel.BasicConsume(
				consumer: _consumer,
				queue: _replyQueueName,
				autoAck: true);

			var response = _respQueue.Take();
			return bool.Parse(response);
		}

		public void Close()
		{
			_connection.Close();
		}
	}
}