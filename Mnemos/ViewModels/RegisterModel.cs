﻿using System.ComponentModel.DataAnnotations;

namespace Mnemos.ViewModels
{
	public class RegisterModel
	{
		[Required(ErrorMessage = "Enter login")]
		[MaxLength(100, ErrorMessage = "Login must be no longer than 100 characters")]
		public string Login { get; set; }

		[Required(ErrorMessage = "Enter email")]
		[EmailAddress(ErrorMessage = "Enter correct email")]
		[MaxLength(100, ErrorMessage = "Email must be no longer than 100 characters")]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		[Required(ErrorMessage = "Enter password")]
		[MaxLength(25, ErrorMessage = "Password must be no longer than 25 characters")]
		[DataType(DataType.Password)]
		public string Password { get; set; }

		[DataType(DataType.Password)]
		[Compare("Password", ErrorMessage = "Enter correct password")]
		[MaxLength(25, ErrorMessage = "Password must be no longer than 25 characters")]
		public string ConfirmPassword { get; set; }
	}
}