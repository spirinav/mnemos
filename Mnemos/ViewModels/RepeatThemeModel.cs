﻿using Mnemos.Models;

namespace Mnemos.ViewModels
{
	public class RepeatThemeModel
	{
		public Theme Theme { get; set; }

		public int TotalQuestions { get; set; }

		public int RepeatQuestions { get; set; }
	}
}