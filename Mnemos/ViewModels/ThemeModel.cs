﻿using System.ComponentModel.DataAnnotations;

namespace Mnemos.ViewModels
{
	public class ThemeModel
	{
		[Required(ErrorMessage = "Enter Name")]
		[MaxLength(200, ErrorMessage = "Name must be no longer than 200 characters")]
		public string Name { get; set; }

		[Required(ErrorMessage = "Enter Description")]
		public string Description { get; set; }
	}
}