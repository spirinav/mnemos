﻿using System.ComponentModel.DataAnnotations;

namespace Mnemos.ViewModels
{
	public class QuestionModal
	{
		[Required(ErrorMessage = "Enter Theme")]
		public int ThemeId { get; set; }

		[Required(ErrorMessage = "Enter Question")]
		public string Text { get; set; }

		[Required(ErrorMessage = "Enter Answer")]
		public string Answer { get; set; }
	}
}