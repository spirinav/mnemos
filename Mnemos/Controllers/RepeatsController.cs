﻿using System;
using System.Linq;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

using Mnemos.Models;
using Mnemos.Models.Interfaces;
using Mnemos.ViewModels;

namespace Mnemos.Controllers
{
	[Authorize]
	public class RepeatsController : Controller
	{
		private readonly IUnitOfWork _unitOfWork;

		public RepeatsController(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public IActionResult Index()
		{
			var themes = _unitOfWork.Themes.FindWithIncludes(t => t.User.Login == HttpContext.User.Identity.Name);

			var model = themes.Select(t => new RepeatThemeModel
			{
				Theme = t,
				TotalQuestions = t.Questions.Count,
				RepeatQuestions = t.Questions.SelectMany(q => q.Repeats).Count(r => r.Date <= DateTime.Now && r.Difficulty == 0)
			}).ToList();

			return View(model);
		}

		public IActionResult Repeat(int id)
		{
			var repeats = _unitOfWork.Repeats.FindWithIncludes(r => r.Question.Theme.Id == id);

			var nextRepeat = repeats
				.Where(r => r.Date <= DateTime.Now && r.Difficulty == 0)
				.OrderBy(r => r.Date)
				.FirstOrDefault();

			if (nextRepeat == null)
			{
				return RedirectToAction("NoQuestion", "Repeats");
			}

			return View(nextRepeat);
		}

		public IActionResult Difficulty(int? repeatId)
		{
			if (repeatId == null)
			{
				return NotFound();
			}

			return View(_unitOfWork.Repeats.FindWithIncludes(r => r.Id == repeatId).FirstOrDefault());
		}

		public IActionResult SaveAndRepeat(int? repeatId, Difficulty difficulty)
		{
			if (repeatId == null)
			{
				return NotFound();
			}

			var repeat = _unitOfWork.Repeats.FindWithIncludes(r => r.Id == repeatId).FirstOrDefault();
			if (repeat == null)
			{
				return NotFound();
			}
			
			repeat.Difficulty = (int) difficulty;
			_unitOfWork.Repeats.Update(repeat);

			var newRepeat = new Repeat
			{
				QuestionId = repeat.QuestionId,
				RepeatNumber = difficulty == Models.Difficulty.Hard ? 0 : repeat.RepeatNumber + 1
			};

			newRepeat.Date = DateTime.Now.Date.AddDays((int) difficulty * newRepeat.RepeatNumber + 1);

			_unitOfWork.Repeats.Add(newRepeat);
			_unitOfWork.Complete();

			return RedirectToAction("Repeat", new RouteValueDictionary(
				new { controller = "Repeats", action = "Repeat", Id = repeat.Question.ThemeId }));
		}

		public IActionResult NoQuestion()
		{
			return View();
		}
	}
}