﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

using Mnemos.Models;
using Mnemos.Models.Interfaces;
using Mnemos.RmqClients;
using Mnemos.ViewModels;

namespace Mnemos.Controllers
{
	public class AccountController : Controller
	{
		private readonly IUnitOfWork _unitOfWork;

		public AccountController(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		[HttpGet]
		public IActionResult Login()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(LoginModel model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			var rpcClient = new RpcClient();
			var response = rpcClient.Call(new User {Login = model.Login, Password = model.Password});
			rpcClient.Close();

			if (response)
			{
				await Authenticate(model.Login);

				return RedirectToAction("Index", "Home");
			}

			ModelState.AddModelError("", "Incorrect login and/or password");

			return View(model);
		}

		[HttpGet]
		public IActionResult Register()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Register(RegisterModel model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			var user = await _unitOfWork.Users.FirstOrDefaultAsync(u => u.Login == model.Login);
			if (user == null)
			{
				_unitOfWork.Users.Add(new User
				{
					Login = model.Login,
					Password = model.Password,
					Email = model.Email,
				});

				_unitOfWork.Complete();

				await Authenticate(model.Login);

				return RedirectToAction("Index", "Home");
			}
			else
				ModelState.AddModelError("", "Incorrect login and/or password");

			return View(model);
		}

		private async Task Authenticate(string userName)
		{
			var claims = new List<Claim> {new(ClaimsIdentity.DefaultNameClaimType, userName)};
			var id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
				ClaimsIdentity.DefaultRoleClaimType);
			await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
		}

		public async Task<IActionResult> Logout()
		{
			await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
			return RedirectToAction("Login", "Account");
		}
	}
}