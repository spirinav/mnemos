﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using Mnemos.Models;
using Mnemos.Models.Interfaces;
using Mnemos.ViewModels;

namespace Mnemos.Controllers
{
	[Authorize]
	public class ThemesController : Controller
	{
		private readonly IUnitOfWork _unitOfWork;

		public ThemesController(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public IActionResult Index()
		{
			return View(_unitOfWork.Themes.Find(t => t.User.Login == HttpContext.User.Identity.Name));
		}

		[HttpGet]
		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create(ThemeModel modal)
		{
			if (!ModelState.IsValid)
			{
				return View(modal);
			}

			var user = await _unitOfWork.Users.FirstOrDefaultAsync(u => u.Login == HttpContext.User.Identity.Name);
			var theme = new Theme
			{
				UserId = user.Id,
				Name = modal.Name,
				Description = modal.Description
			};

			_unitOfWork.Themes.Add(theme);
			_unitOfWork.Complete();

			return RedirectToAction(nameof(Index));
		}

		[HttpGet]
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var theme = await _unitOfWork.Themes.GetByIdAsync((int) id);
			if (theme == null)
			{
				return NotFound();
			}

			return View(new ThemeModel {Name = theme.Name, Description = theme.Description});
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, ThemeModel model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			var theme = await _unitOfWork.Themes.GetByIdAsync(id);
			if (theme == null)
			{
				return NotFound();
			}

			theme.Name = model.Name;
			theme.Description = model.Description;

			_unitOfWork.Themes.Update(theme);
			_unitOfWork.Complete();

			return RedirectToAction(nameof(Index));
		}

		[HttpGet]
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var theme = await _unitOfWork.Themes.GetByIdAsync((int) id);

			if (theme == null)
			{
				return NotFound();
			}

			return View(theme);
		}

		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var theme = await _unitOfWork.Themes.GetByIdAsync(id);
			_unitOfWork.Themes.Remove(theme);
			_unitOfWork.Complete();
			return RedirectToAction(nameof(Index));
		}
	}
}