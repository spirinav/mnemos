﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

using Mnemos.Models;
using Mnemos.Models.Interfaces;
using Mnemos.ViewModels;

namespace Mnemos.Controllers
{
	[Authorize]
	public class QuestionsController : Controller
	{
		private readonly IUnitOfWork _unitOfWork;

		public QuestionsController(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public IActionResult Index()
		{
			var questions = _unitOfWork.Questions
				.FindWithIncludes(q => q.Theme.User.Login == HttpContext.User.Identity.Name);

			return View(questions);
		}

		public IActionResult IndexByTheme(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var theme = _unitOfWork.Themes.GetById((int) id);
			if (theme == null)
			{
				return NotFound();
			}

			var questions = _unitOfWork.Questions.FindWithIncludes(q => q.Theme.Equals(theme));

			ViewData["ThemeName"] = theme.Name;
			return View(questions);
		}

		[HttpGet]
		public async Task<IActionResult> Details(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var question = _unitOfWork.Questions
				.FindWithIncludes(q => q.Id == (int) id)
				.FirstOrDefault();
			if (question == null)
			{
				return NotFound();
			}

			return View(question);
		}

		[HttpGet]
		public IActionResult Create(int? id)
		{
			var themes = new List<Theme>();
			var question = new QuestionModal();

			if (id == null)
			{
				themes.AddRange(_unitOfWork.Themes.Find(x => x.User.Login == HttpContext.User.Identity.Name));
				ViewData["IsThemeSelected"] = false;
			}
			else
			{
				themes.Add(_unitOfWork.Themes.GetById((int)id));
				ViewData["IsThemeSelected"] = true;
				question.ThemeId = (int)id;
			}

			ViewData["ThemeId"] = new SelectList(themes, "Id", "Name");

			return View(question);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create(QuestionModal model)
		{
			if (!ModelState.IsValid)
			{
				return RedirectToAction("Create", "Questions", new {id = model.ThemeId});
			}

			var question = new Question
			{
				ThemeId = model.ThemeId,
				Text = model.Text,
				Answer = model.Answer,
				Repeats = new List<Repeat> {new() {Date = DateTime.Now.Date.AddDays(1)}}
			};

			_unitOfWork.Questions.Add(question);
			_unitOfWork.Complete();

			return RedirectToAction("IndexByTheme", "Questions", new { id = model.ThemeId });
		}

		[HttpGet]
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var question = await _unitOfWork.Questions.GetByIdAsync((int) id);
			if (question == null)
			{
				return NotFound();
			}

			var questionModal = new QuestionModal
			{
				ThemeId = question.ThemeId,
				Text = question.Text,
				Answer = question.Answer
			};

			ViewData["ThemeId"] = new SelectList(_unitOfWork.Themes.GetAll(), "Id", "Name", question.ThemeId);

			return View(questionModal);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, QuestionModal model)
		{
			if (!ModelState.IsValid)
			{
				ViewData["ThemeId"] = new SelectList(_unitOfWork.Themes.GetAll(), "Id", "Name", model.ThemeId);
				return View(model);
			}

			var question = await _unitOfWork.Questions.GetByIdAsync(id);
			if (question == null)
			{
				return NotFound();
			}

			question.ThemeId = model.ThemeId;
			question.Text = model.Text;
			question.Answer = model.Answer;

			_unitOfWork.Questions.Update(question);
			_unitOfWork.Complete();

			return RedirectToAction("IndexByTheme", "Questions", new { id = model.ThemeId });
		}

		[HttpGet]
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var question = _unitOfWork.Questions.FindWithIncludes(q => q.Id == (int) id).FirstOrDefault();
			if (question == null)
			{
				return NotFound();
			}

			return View(question);
		}

		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var question = await _unitOfWork.Questions.GetByIdAsync(id);
			_unitOfWork.Questions.Remove(question);
			_unitOfWork.Complete();
			return RedirectToAction(nameof(Index));
		}
	}
}