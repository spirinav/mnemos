﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Mnemos.Models.Interfaces;
using Mnemos.ViewModels;

namespace Mnemos.Controllers
{
	public class HomeController : Controller
	{
		private readonly IUnitOfWork _unitOfWork;

		public HomeController(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		[Authorize]
		public IActionResult Index()
		{
			var themes = _unitOfWork.Themes
				.FindWithIncludes(t => t.User.Login == HttpContext.User.Identity.Name)
				.ToList();

			var model = new StatisticViewModel
			{
				TotalNumberTheme = themes.Count,
				TotalNumberQuestion = themes.SelectMany(t => t.Questions).Count(),
				TotalNumberQuestionToRepeat = themes.SelectMany(t => t.Questions)
					.SelectMany(q => q.Repeats)
					.Count(r => r.Date <= DateTime.Now && r.Difficulty == 0),
				MaxQuestionTheme = themes.Select(t => new {Theme = t, t.Questions.Count})
					.OrderByDescending(x => x.Count)
					.FirstOrDefault()?
					.Theme?.Name,
				MaxQuestionToRepeatTheme = themes.Select(t => new
					{
						Theme = t,
						Count = t.Questions.SelectMany(q => q.Repeats)
							.Count(r => r.Date <= DateTime.Now && r.Difficulty == 0)
					})
					.Where(x => x.Count > 0)
					.OrderByDescending(x => x.Count)
					.FirstOrDefault()?
					.Theme?.Name,
				MostRepeatedQuestion = themes.SelectMany(t => t.Questions).Select(q => new
					{
						Question = q.Text,
						q.Repeats.Count
					}).Where(x => x.Count > 0)
					.OrderByDescending(x => x.Count)
					.FirstOrDefault()?
					.Question
			};

			return View(model);
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
		}
	}
}